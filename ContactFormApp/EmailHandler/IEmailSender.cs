﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactForm.EmailHandler
{
    public interface IEmailSender
    {
        void SendEmail();
    }
}
