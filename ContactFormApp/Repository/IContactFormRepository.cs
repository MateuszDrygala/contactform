﻿using ContactForm.Models;

namespace ContactForm.Repository
{
    public interface IContactFormRepository
    {
        void SaveContactForm(Contact contactForm);
    }
}
