﻿using System.ComponentModel.DataAnnotations;

namespace ContactForm.Models
{
    public class Contact
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "This field cannot be empty")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "This field cannot be empty")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "This field cannot be empty")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "This field cannot be empty")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"\(?\d{3}\)?-? *\d{3}-? *-?\d{3,4}", ErrorMessage = "Not a valid phone number. Recommended format: 000-000-0000 or 000-000-000")]
        public int PhoneNumber { get; set; }

        public string AreaOfInterest { get; set; }

        [Required(ErrorMessage = "This field cannot be empty")]
        public string Message { get; set; }

    }
}
