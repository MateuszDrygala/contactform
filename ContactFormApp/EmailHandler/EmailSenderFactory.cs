﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactForm.EmailHandler
{
    public class EmailSenderFactory
    {
        public static IEmailSender CreateGmailSender(EmailInformationData emailInformationData)
        {
            return new GmailSender(emailInformationData);
        }
    }
}
