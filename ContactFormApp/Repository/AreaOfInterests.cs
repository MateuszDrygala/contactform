﻿namespace ContactForm.Repository
{
    public enum AreaOfInterests
    {
        Question,
        Complaint,
        Help,
        Other
    }
}
