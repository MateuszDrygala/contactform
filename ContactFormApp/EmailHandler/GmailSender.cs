﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ContactForm.EmailHandler
{
    public class GmailSender : IEmailSender
    {
        private readonly EmailInformationData _emailInformationData;

        public GmailSender(EmailInformationData emailInformationData)
        {
            _emailInformationData = emailInformationData;
        }

        public void SendEmail()
        {
            try
            {
                SmtpClient smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(_emailInformationData.SendersAddress, _emailInformationData.SendersPassword),
                    Timeout = 3000
                };

                MailMessage message = new MailMessage(_emailInformationData.SendersAddress,
                    _emailInformationData.ReceiversAddress, _emailInformationData.Subject, _emailInformationData.Body);

                smtp.Send(message);
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
