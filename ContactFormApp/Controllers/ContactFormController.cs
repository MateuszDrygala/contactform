﻿using ContactForm.EmailHandler;
using ContactForm.Models;
using ContactForm.Repository;
using Microsoft.AspNetCore.Mvc;

namespace ContactForm.Controllers
{
    public class ContactFormController : Controller
    {
        private readonly IContactFormRepository _repository;

        public ContactFormController(IContactFormRepository repository)
        {
            _repository = repository;
        }

        public IActionResult AddContact()
        {
            Contact model = new Contact();

            return View(model);
        }

        [HttpPost]
        public IActionResult AddContact(Contact model)
        {
            if (ModelState.IsValid)
            {
                _repository.SaveContactForm(model);
                TempData["message"] = $"Wysłano {model.EmailAddress}.";

                var gmailSender = EmailSenderFactory.CreateGmailSender(
                    new EmailInformationData(EmailInformationConstants.SendersAddress,
                    EmailInformationConstants.Subject, EmailInformationConstants.Body,
                    EmailInformationConstants.SendersPassword, model.EmailAddress));

                try
                {
                    gmailSender.SendEmail();
                }
                catch
                {
                    return RedirectToAction("Index", "Home");
                }

                return RedirectToAction("ThankYouPage", model);
            }
            else
            {

                return View(model);
            }
        }

        public IActionResult ThankYouPage(Contact contact)
        {
            return View(contact);
        }
    }
}