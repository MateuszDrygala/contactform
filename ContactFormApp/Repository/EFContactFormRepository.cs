﻿using ContactForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactForm.Repository
{
    public class EFContactFormRepository : IContactFormRepository
    {
        private readonly ApplicationDbContext _context;

        public EFContactFormRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void SaveContactForm(Contact contactForm)
        {
            if (contactForm.Id == 0)
            {
                _context.ContactForms.Add(contactForm);
            }

            _context.SaveChanges();
        }
    }
}
