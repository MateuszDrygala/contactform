﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContactForm.EmailHandler
{
    public class EmailInformationData
    {

        public EmailInformationData(string sendersAddress, string subject, string body, string sendersPassword, string receiversAddress)
        {
            SendersAddress = sendersAddress;
            Subject = subject;
            Body = body;
            SendersPassword = sendersPassword;
            ReceiversAddress = receiversAddress;
        }

        public string SendersAddress { get; }
        public string Subject { get; }
        public string Body { get; }
        public string SendersPassword { get; }
        public string ReceiversAddress { get; }
    }
}
